# Generated by CMake 3.1.0-rc1

if("${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION}" LESS 2.5)
   message(FATAL_ERROR "CMake >= 2.6.0 required")
endif()
cmake_policy(PUSH)
cmake_policy(VERSION 2.6)
#----------------------------------------------------------------
# Generated CMake target import file.
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Protect against multiple inclusion, which would fail when already imported targets are added once more.
set(_targetsDefined)
set(_targetsNotDefined)
set(_expectedTargets)
foreach(_expectedTarget jansson)
  list(APPEND _expectedTargets ${_expectedTarget})
  if(NOT TARGET ${_expectedTarget})
    list(APPEND _targetsNotDefined ${_expectedTarget})
  endif()
  if(TARGET ${_expectedTarget})
    list(APPEND _targetsDefined ${_expectedTarget})
  endif()
endforeach()
if("${_targetsDefined}" STREQUAL "${_expectedTargets}")
  set(CMAKE_IMPORT_FILE_VERSION)
  cmake_policy(POP)
  return()
endif()
if(NOT "${_targetsDefined}" STREQUAL "")
  message(FATAL_ERROR "Some (but not all) targets in this export set were already defined.\nTargets Defined: ${_targetsDefined}\nTargets not yet defined: ${_targetsNotDefined}\n")
endif()
unset(_targetsDefined)
unset(_targetsNotDefined)
unset(_expectedTargets)


# Create imported target jansson
add_library(jansson SHARED IMPORTED)

# Import target "jansson" for configuration "Debug"
set_property(TARGET jansson APPEND PROPERTY IMPORTED_CONFIGURATIONS DEBUG)
set_target_properties(jansson PROPERTIES
  IMPORTED_IMPLIB_DEBUG "F:/Development/dhole/external/jansson-2.7/lib/Debug/jansson_d.lib"
  IMPORTED_LOCATION_DEBUG "F:/Development/dhole/external/jansson-2.7/bin/Debug/jansson_d.dll"
  )

# Import target "jansson" for configuration "Release"
set_property(TARGET jansson APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(jansson PROPERTIES
  IMPORTED_IMPLIB_RELEASE "F:/Development/dhole/external/jansson-2.7/lib/Release/jansson.lib"
  IMPORTED_LOCATION_RELEASE "F:/Development/dhole/external/jansson-2.7/bin/Release/jansson.dll"
  )

# Import target "jansson" for configuration "MinSizeRel"
set_property(TARGET jansson APPEND PROPERTY IMPORTED_CONFIGURATIONS MINSIZEREL)
set_target_properties(jansson PROPERTIES
  IMPORTED_IMPLIB_MINSIZEREL "F:/Development/dhole/external/jansson-2.7/lib/MinSizeRel/jansson.lib"
  IMPORTED_LOCATION_MINSIZEREL "F:/Development/dhole/external/jansson-2.7/bin/MinSizeRel/jansson.dll"
  )

# Import target "jansson" for configuration "RelWithDebInfo"
set_property(TARGET jansson APPEND PROPERTY IMPORTED_CONFIGURATIONS RELWITHDEBINFO)
set_target_properties(jansson PROPERTIES
  IMPORTED_IMPLIB_RELWITHDEBINFO "F:/Development/dhole/external/jansson-2.7/lib/RelWithDebInfo/jansson.lib"
  IMPORTED_LOCATION_RELWITHDEBINFO "F:/Development/dhole/external/jansson-2.7/bin/RelWithDebInfo/jansson.dll"
  )

# This file does not depend on other imported targets which have
# been exported from the same project but in a separate export set.

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
cmake_policy(POP)
