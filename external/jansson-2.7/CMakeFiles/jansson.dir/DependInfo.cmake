# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "C"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_C
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/dump.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/dump.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/error.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/error.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/hashtable.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/hashtable.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/hashtable_seed.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/hashtable_seed.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/load.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/load.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/memory.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/memory.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/pack_unpack.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/pack_unpack.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/strbuffer.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/strbuffer.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/strconv.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/strconv.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/utf.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/utf.c.o"
  "/home/efarhan/Development/Dhole/external/jansson-2.7/src/value.c" "/home/efarhan/Development/Dhole/external/jansson-2.7/CMakeFiles/jansson.dir/src/value.c.o"
  )
SET(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
SET(CMAKE_TARGET_DEFINITIONS
  "HAVE_CONFIG_H"
  "JANSSON_USING_CMAKE"
  )

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  "include"
  "external/jansson-2.7/include"
  "external/jansson-2.7/private_include"
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
