project(lua C)

add_definitions("-D_CRT_SECURE_NO_WARNINGS")

file(GLOB lua_SRC src/*.h src/*.c)

list(REMOVE_ITEM lua_SRC src/lua.c src/luac.c)

add_library(lua STATIC ${lua_SRC})


