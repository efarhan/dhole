#include <Engine/Const.hpp>
#include <Engine/Init.hpp>

void * reference[LAST_REF-1];
int main(void)
{
	for(int i = 0; i<LAST_REF-1;i++)
	{
		reference[i] = NULL;
	}
	int status = 0;

	Engine engine = Engine();
	reference[ENGINE] = (void*)(&engine);
	status = engine.run();
	return status;
}
