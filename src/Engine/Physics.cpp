/*
 * Physics.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#include <Engine/Physics.hpp>


PhysicsManager::PhysicsManager (): world(NULL)
{
	reference[PHYSICS] = (void*)this;
}

PhysicsManager::~PhysicsManager()
{
	if(world != NULL)
		exitWorld();
}

int PhysicsManager::initWorld(double gx, double gy)
{
	if(world != NULL)
		exitWorld();
	world = new b2World(b2Vec2(gx, gy));
	return 0;
}

int PhysicsManager::exitWorld()
{
	if (world != NULL)
	{
		delete(world);
		world = NULL;
	}
	return 0;
}
