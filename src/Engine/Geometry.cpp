/*
 * Vector2.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */



#include <Engine/Geometry.hpp>

Vector2::Vector2(double x, double y)
{
	this->x = x;
	this->y = y;
	this->length = sqrt(x*x+y*y);
}
Vector2::Vector2()
{
	this->x = 0.0;
	this->y = 0.0;
	this->length = 0.0;
}
Vector2::~Vector2()
{
}

double Vector2::getX()
{
	return this->x;
}

double Vector2::getY()
{
	return this->y;
}

void Vector2::setX(double x)
{
	this->x = x;
}

void Vector2::setY(double y)
{
	this->y = y;
}
Vector2 Vector2::operator+(Vector2 p)
{
	return Vector2(this->x + p.getX(), this->y + p.getY());
}
Rect Vector2::operator+(Rect s)
{
	return Rect(s.getPos() + *(this), s.getSize());
}
Vector2 Vector2::operator-(Vector2 p)
{
	return Vector2(this->x - p.getX(), this->y - p.getY());
}

bool Vector2::operator==(Vector2 p)
{
	return (this->getX() == p.getX()) && (this->getY() == p.getY());
}
bool Vector2::operator!=(Vector2 p)
{
	return !((this->getX() == p.getX()) && (this->getY() == p.getY()));
}
bool Vector2::operator<(Vector2 p)
{
	return (this->normalize() < p.normalize());
}
bool Vector2::operator>(Vector2 p)
{
	return (this->normalize() > p.normalize());
}
Vector2 Vector2::operator /(double d)
{
	return Vector2(x / d, y / d);
}
bool Vector2::isIn(Rect s)
{
	return s.isIn(*this);
}

bool Vector2::isIn(Vector2 pos, Vector2 end)
{
	return Rect(pos, end).isIn(*this);
}
Vector2 Vector2::normalize()
{
	return (*this)/length;
}


Rect::Rect(Vector2 newX, Vector2 newY) :
		pos(newX), size(newY)
{
	this->checkVector2();
}

Rect::~Rect()
{
}

Vector2 Rect::getPos()
{
	return (this->pos);
}

Vector2 Rect::getSize()
{
	return (this->size);
}

void Rect::setPos(Vector2 newX)
{
	this->pos = newX;
	checkVector2();
}

void Rect::setSize(Vector2 newY)
{
	this->size = newY;
	checkVector2();
}

bool Rect::isIn(Rect other)
{
	bool status[] =
	{ (this->pos.getX() > (other.getPos()+other.getSize()).getX())
			|| ((this->pos+this->size).getX() < other.getPos().getX()),
			(this->pos.getY() > (other.getPos()+other.getSize()).getY())
					|| ((this->pos+this->size).getY() < other.getPos().getY()) };
	return !(status[0] || status[1]);
}

bool Rect::isIn(Vector2 x, Vector2 y)
{
	return (this->isIn(Rect(x, y)));
}

bool Rect::isIn(Vector2 p)
{
	return this->isInX(p.getX()) && this->isInY(p.getY());
}

void Rect::checkVector2()
{
	//To make sure that the pos and the end are placed correctly
	if (size.getX() < 0)
	{
		pos.setX(pos.getX()+size.getX());
		size.setX(-size.getX());
	}

	if (size.getY() < 0)
	{
		pos.setY(pos.getY()+size.getY());
		size.setY(-size.getY());
	}
}

bool Rect::isInX(double x)
{
	return (this->pos.getX()+this->size.getX() > x) && (this->pos.getX() < x);
}

bool Rect::isInY(double y)
{
	return (this->pos.getY() < y) && (this->pos.getY()+this->size.getY() > y);
}

bool Rect::operator==(Rect s2)
{
	return (this->pos == s2.pos) && (this->size == s2.size);
}
Rect Rect::operator +(Rect s2)
{
	return Rect(this->pos + s2.getPos(), this->size + s2.getSize());
}
Rect Rect::operator +(Vector2 p)
{
	return Rect(this->pos + p, this->size);
}



