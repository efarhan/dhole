/*
 * Init.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */
#include <Engine/Init.hpp>

Engine::Engine()
{
	physics = NULL;
	window = NULL;
	imgManager = NULL;
	scriptExport = NULL;
#ifdef EXPORT_LUA
	scriptExport = new LuaExport();
#endif
}

Engine::~Engine()
{
	if(window != NULL)
	{
		if(window->isOpen())
		{
			window->close();
		}
		delete(window);
		window = NULL;
	}

	scriptExport->exit();
	delete(scriptExport);

	if(physics != NULL)
	{
		delete(physics);
	}
	if(this->imgManager != NULL)
	{
		imgManager->clearImg();
		delete(imgManager);
		imgManager = NULL;
	}
}

int Engine::run()
{

	this->scriptExport->init();

	if (window == NULL)
	{

		this->scriptExport->exit();

		return 1;
	}
	sf::Clock clock;
	while (window->isOpen())
	{

		sf::Event event;
		while (window->pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window->close();

		}
		sf::Time elapsed = clock.restart();
		// Clear screen
		window->clear();

		this->scriptExport->update(elapsed.asSeconds());


		// Update the window
		window->display();

	}



	this->scriptExport->exit();

	return 0;
}

int Engine::initWindow(Vector2 gameSize)
{
	this->gameSize = gameSize;

	sf::VideoMode desktop = sf::VideoMode::getDesktopMode();
	window = new sf::RenderWindow(sf::VideoMode(1024, 768, desktop.bitsPerPixel), "Dhose window");
	/*TODO: if debug set screen to 800, 600

	        self.screen = sfml.RenderWindow(desktop, 'Kudu Window', style)
	        log(self.screen.settings)
	        self.real_screen_size = Vector2(self.screen.size)
	        self.screen_diff_ratio = self.real_screen_size / self.screen_size
	        self.screen.vertical_synchronization = True
	*/
	return 0;
}

int Engine::exitWindow()
{
	if (window != NULL)
	{
		if (window->isOpen())
		{
			window->close();
		}
		delete(window);
		window = NULL;
	}
	return 0;
}

int Engine::setPhysics(PhysicsManager* physics)
{
	this->physics = physics;
	return 0;
}
int Engine::setImgManager(ImgManager*)
{
	this->imgManager = imgManager;
	return 0;
}



