/*
 * DholeExport.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */
#include <LuaExport/DholeExport.hpp>



static const struct luaL_Reg dholeLib [] = {
	{"initWindow", l_initWindow},
	{"exitWindow", l_exitWindow},
	{"initPhysics", l_initPhysics},
	{"exitPhysics", l_exitPhysics},
	{"initImg", l_initImg},
	{"exitImg", l_exitImg},
	{NULL, NULL}
};



int luaopenDhole (lua_State *L)
{
	lua_newtable(L);
	luaL_setfuncs(L, dholeLib, 0);
	lua_setglobal(L, "Dhole");

	return 0;
}

int l_initWindow (lua_State *L)
{
	(void) L;
	int argc = lua_gettop(L);
	if (argc != 2)
	{
		std::cerr << "Expected 2 arguments for initWindow\n";
		return 1;
	}
	double sizeX = lua_tonumber(L, 1);
	double sizeY = lua_tonumber(L, 2);

	reinterpret_cast<Engine*>(reference[ENGINE])->initWindow(Vector2(sizeX, sizeY));

	return 0;
}

int l_exitWindow (lua_State *L)
{
	(void) L;
	reinterpret_cast<Engine*>(reference[ENGINE])->exitWindow();
	return 0;
}

int l_initPhysics(lua_State *L)
{
	(void)L;
	reinterpret_cast<Engine*>(reference[ENGINE])->setPhysics(new PhysicsManager());
	reinterpret_cast<PhysicsManager*>(reference[PHYSICS])->initWorld(0.0,0.0);
	return 0;
}

int l_exitPhysics(lua_State *L)
{
	(void)L;
	reinterpret_cast<PhysicsManager*>(reference[PHYSICS])->exitWorld();
	return 0;
}

int l_initImg(lua_State *L)
{
	(void)L;
	reinterpret_cast<Engine*>(reference[ENGINE])->setImgManager(new ImgManager());
	return 0;
}

int l_exitImg(lua_State *L)
{
	(void)L;
	reinterpret_cast<ImgManager*>(reference[IMG_MANAGER])->clearImg();
	return 0;
}

int l_initConst(lua_State *L)
{
	(void) L;
	return 0;
}


