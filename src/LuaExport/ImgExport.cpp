/*
 * ImgExport.cpp
 *
 *  Created on: Dec 21, 2014
 *      Author: efarhan
 */
#ifdef EXPORT_LUA

#include <LuaExport/ImgExport.hpp>

static const struct luaL_Reg imgLib [] = {
	{"addTexture", l_addTexture},
	{"getSprite", l_getSprite},
	{"removeTexture", l_removeTexture},
	{NULL, NULL}
};



int luaopenImg (lua_State *L)
{
	lua_newtable(L);
	lua_setglobal(L, "Img");
	luaL_setfuncs(L, imgLib, 0);

	return 0;
}

int l_addTexture(lua_State *L)
{
	(void)L;
	return 0;
}

int l_getSprite(lua_State *L)
{
	(void)L;
	return 0;
}

int l_removeTexture(lua_State *L)
{
	(void)L;
	return 0;
}
#endif



