/*
 * LuaExport.cpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */
#include <LuaExport/LuaExport.hpp>

LuaExport::LuaExport(): state(NULL)
{

}

LuaExport::~LuaExport()
{
	this->exit();
}

int LuaExport::init()
{
	state = luaL_newstate();
	if (state == NULL)
	{
		return 1;
	}
	luaopen_io(state); // provides io.*
	luaopen_base(state);
	luaopen_table(state);
	luaopen_string(state);
	luaopen_math(state);

	//Init Dhole lib
	luaopenDhole (state);

	int status = luaL_loadfile(state, "script/main.lua");
	if (status == 0)
	{
		//execute program
		status = lua_pcall(state, 0, LUA_MULTRET, 0);
		if (status != 0) {
			//show error
		    std::cerr << "-- " << lua_tostring(state, -1) << std::endl;
		    lua_pop(state, 1); // remove error message
		}
	}
	lua_getglobal(state, "init");
	lua_call(state,0,0);

	return status;
}

int LuaExport::reset()
{
	int status = 0;
	status = exit();
	status = status && init();

	return status;
}

int LuaExport::exit()
{
	if(state != NULL)
	{
		lua_getglobal(state, "exit");
		lua_call(state,0,0);
		lua_close(state);
		state = NULL;
	}

	return 0;
}

int LuaExport::update(float deltaTime)
{
	lua_getglobal(state, "update");
	lua_pushnumber(state, (double)deltaTime);
	lua_call(state,1,0);
	return 0;
}




