/*
 * Img.hpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_ENGINE_IMG_HPP_
#define INCLUDE_ENGINE_IMG_HPP_

#include <list>
#include <SFML/Graphics.hpp>

class ImgManager
{
public:
	ImgManager();
	~ImgManager();
	void* addImage(const char* path);
	int removeImage(void*);
	int clearImg();

private:
	std::list<sf::Texture> images;
};

#endif /* INCLUDE_ENGINE_IMG_HPP_ */
