/*
 * Init.hpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_ENGINE_INIT_HPP_
#define INCLUDE_ENGINE_INIT_HPP_


#include <Engine/Export.hpp>
#include <Engine/Geometry.hpp>
#include <Engine/Physics.hpp>
#include <Engine/Font.hpp>
#include <Engine/Img.hpp>
#include <Engine/Snd.hpp>

#ifdef EXPORT_LUA
#include <LuaExport/LuaExport.hpp>
#endif


#include <SFML/Graphics.hpp>

class Engine
{
public:
	Engine();
	~Engine();
	int run();
	int initWindow(Vector2);
	int exitWindow();
	int setPhysics(PhysicsManager*);
	int setImgManager(ImgManager*);
	int setFontManager(FontManager*);
	int setSndManager(SndManager*);
private:
	Export* scriptExport;
	PhysicsManager* physics;
	ImgManager* imgManager;
	sf::RenderWindow* window;
	Vector2 gameSize;
};


#endif /* INCLUDE_ENGINE_INIT_HPP_ */
