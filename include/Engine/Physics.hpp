/*
 * Physics.hpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_ENGINE_PHYSICS_HPP_
#define INCLUDE_ENGINE_PHYSICS_HPP_

#include <Engine/Const.hpp>
#include <Box2D/Box2D.h>

class PhysicsManager
{
public:
	PhysicsManager();
	~PhysicsManager();
	int initWorld(double gx, double gy);
	int exitWorld();

	void* addBody();
	int removeBody(void*);

	int addBox(void* body);
	int addCircle(void* body);
private:
	b2World* world;
};

#endif /* INCLUDE_ENGINE_PHYSICS_HPP_ */
