/*
 * Export.hpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_ENGINE_EXPORT_HPP_
#define INCLUDE_ENGINE_EXPORT_HPP_

class Export
{
public:
	virtual ~Export();
	virtual int init() = 0;
	virtual int update(float deltaTime) = 0;
	virtual int exit() = 0;

};



#endif /* INCLUDE_ENGINE_EXPORT_HPP_ */
