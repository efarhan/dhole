/*
 * Vector2.hpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_ENGINE_GEOMETRY_HPP_
#define INCLUDE_ENGINE_GEOMETRY_HPP_

#include <cmath>

class Rect;

class Vector2
{
public:
	Vector2();
	Vector2(double x, double y);
	~Vector2();
	double getX();
	double getY();
	void setX(double x);
	void setY(double y);
	Vector2 operator +(Vector2);
	Rect operator +(Rect);
	Vector2 operator -(Vector2);
	bool operator ==(Vector2);
	bool operator !=(Vector2);
	bool operator <(Vector2);
	bool operator >(Vector2);
	Vector2 operator /(double);
	bool isIn(Rect s);
	bool isIn(Vector2 origin, Vector2 end);
	Vector2 normalize();

private:
	double x;
	double y;
	double length;
};

class Rect
{
public:
	Rect(Vector2 origin, Vector2 end);
	~Rect();
	Vector2 getPos();
	Vector2 getSize();

	void setPos(Vector2);
	void setSize(Vector2);
	bool isIn(Rect);
	bool isIn(Vector2, Vector2);
	bool isIn(Vector2);
	bool isInX(double x);
	bool isInY(double y);
	void checkVector2();

	bool operator ==(Rect);
	Rect operator +(Rect);
	Rect operator +(Vector2);
private:
	Vector2 pos;
	Vector2 size;
};


#endif /* INCLUDE_ENGINE_GEOMETRY_HPP_ */
