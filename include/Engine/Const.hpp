/*
 * Const.hpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_ENGINE_CONST_HPP_
#define INCLUDE_ENGINE_CONST_HPP_

#include <cstddef>
#include <iostream>
#include <map>

typedef enum {ENGINE, IMG_MANAGER, SND_MANAGER, FONT_MANAGER, PHYSICS, LAST_REF} REF;

extern void * reference [];

#endif /* INCLUDE_ENGINE_CONST_HPP_ */
