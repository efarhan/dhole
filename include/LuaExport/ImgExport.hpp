/*
 * ImgExport.hpp
 *
 *  Created on: Dec 21, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_LUAEXPORT_IMGEXPORT_HPP_
#define INCLUDE_LUAEXPORT_IMGEXPORT_HPP_

#include <Engine/Const.hpp>
#include <Engine/Img.hpp>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

int luaopenImg (lua_State *L);

int l_addTexture(lua_State *L);
int l_getSprite(lua_State *L);
int l_removeTexture(lua_State *L);



#endif /* INCLUDE_LUAEXPORT_IMGEXPORT_HPP_ */
