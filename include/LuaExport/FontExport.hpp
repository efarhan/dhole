/*
 * FontExport.hpp
 *
 *  Created on: Dec 21, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_LUAEXPORT_FONTEXPORT_HPP_
#define INCLUDE_LUAEXPORT_FONTEXPORT_HPP_

#include <Engine/Const.hpp>
#include <Engine/Init.hpp>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

int luaopenFont (lua_State *L);



#endif /* INCLUDE_LUAEXPORT_FONTEXPORT_HPP_ */
