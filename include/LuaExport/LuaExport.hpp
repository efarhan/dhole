/*
 * LuaExport.hpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_LUAEXPORT_LUAEXPORT_HPP_
#define INCLUDE_LUAEXPORT_LUAEXPORT_HPP_

#include <Engine/Export.hpp>
#include <LuaExport/DholeExport.hpp>
extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}


class LuaExport: public Export
{
public:
	LuaExport();
	~LuaExport();
	int init();
	int reset();
	int update(float deltaTime);
	int exit();
private:
	lua_State* state;
};



#endif /* INCLUDE_LUAEXPORT_LUAEXPORT_HPP_ */
