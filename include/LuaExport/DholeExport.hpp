/*
 * DholeExport.hpp
 *
 *  Created on: Dec 18, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_LUAEXPORT_DHOLEEXPORT_HPP_
#define INCLUDE_LUAEXPORT_DHOLEEXPORT_HPP_

#include <Engine/Const.hpp>
#include <Engine/Init.hpp>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

int luaopenDhole (lua_State *L);

//Lua functions
int l_initWindow (lua_State *L);
int l_exitWindow (lua_State *L);

int l_initPhysics(lua_State *L);
int l_exitPhysics(lua_State *L);

int l_initImg(lua_State *L);
int l_exitImg(lua_State *L);

int l_initFont(lua_State *L);
int l_exitFont(lua_State *L);




#endif /* INCLUDE_LUAEXPORT_DHOLEEXPORT_HPP_ */
