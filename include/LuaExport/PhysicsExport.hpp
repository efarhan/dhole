/*
 * PhysicsExport.hpp
 *
 *  Created on: Dec 21, 2014
 *      Author: efarhan
 */

#ifndef INCLUDE_LUAEXPORT_PHYSICSEXPORT_HPP_
#define INCLUDE_LUAEXPORT_PHYSICSEXPORT_HPP_

#include <Engine/Const.hpp>
#include <Engine/Physics.hpp>

extern "C" {
	#include "lua.h"
	#include "lauxlib.h"
	#include "lualib.h"
}

int luaopenPhysics(lua_State *L);



#endif /* INCLUDE_LUAEXPORT_PHYSICSEXPORT_HPP_ */
